%% Some parameters to set - make sure that your code works at image borders!
patchSize = 5;
sigma = 20; % standard deviation (different for each image!)
h = 0.4; %decay parameter
searchWindowSize = 13;

imageNoisy = double(rgb2gray(imread('images/alleyNoisy_sigma20.png')));
imageReference = double(rgb2gray(imread('images/alleyReference.png')));

% Pre-processing:
% Store integral images in a cell, which will be mapped to each offset.
% we want as close to constant access as possible - and cells may provide
% hashmap like access time.
% Although if matlab uses pass-by-value then this will slow down the run
% time for the evaluation of each image in "evaluateIntegralImage.m".
integralImages = {};
winWidth = floor(searchWindowSize/2);
winHeight = winWidth;
for x =-winWidth:winWidth
    for y =-winHeight:winHeight
        % Compute the integal image for each window offset.
        integralImage = computeIntegralImage(imageNoisy, x, y, patchSize, searchWindowSize); 
        % % -winWidth+winWidth zero at most initially so + 1 to map this to
        % positive indices.
        idxX = x + winWidth + 1;
        idxY = y + winHeight + 1;
        integralImages{idxX, idxY} = integralImage;
    end
end

tic
filtered = nonLocalMeans(imageNoisy, integralImages, sigma, h, patchSize,searchWindowSize);
toc

% tic
% filtered = naiveNonLocalMeans(imageNoisy, sigma, h, patchSize,searchWindowSize);
% toc


%% Let's show your results!

%Show the denoised image
figure('name', 'NL-Means Denoised Image');
imshow(filtered, []);
 
%Show difference image
diff_image = abs(imageReference - filtered);
figure('name', 'Difference Image');
imshow(diff_image / max(max((diff_image))), []);

%Print some statistics ((Peak) Signal-To-Noise Ratio)
disp('For Noisy Input');
[peakSNR, SNR] = psnr(imageNoisy, imageReference);
disp(['SNR: ', num2str(SNR, 10), '; PSNR: ', num2str(peakSNR, 10)]);

disp('For Denoised Result');
[peakSNR, SNR] = psnr(filtered, imageReference);
disp(['SNR: ', num2str(SNR, 10), '; PSNR: ', num2str(peakSNR, 10)]);

%Feel free (if you like only :)) to use some other metrics (Root
%Mean-Square Error (RMSE), Structural Similarity Index (SSI) etc.)