function [patchSum] = evaluateIntegralImage(ii, row, col, patchSize, windowSize)
patchOffset = floor(patchSize/2);

% Remap to new location (ensures indices exist).
% We could probably find a more efficient way to compute summed area tables
% using bounds checking in the case where the patches lay outside of the
% image - without my padding approach.
newRow = row + 1 + windowSize + patchOffset;
newCol = col + 1 + windowSize + patchOffset;

l1 = ii(newRow  - patchOffset-1, newCol - patchOffset-1);
l2 = ii(newRow - patchOffset-1,  newCol + patchOffset);
l3 = ii(newRow + patchOffset,    newCol + patchOffset);
l4 = ii(newRow + patchOffset,    newCol - patchOffset-1);
patchSum = l3 - l2 - l4 + l1;
end