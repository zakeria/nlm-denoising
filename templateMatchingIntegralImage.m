function [offsetsRows, offsetsCols, distances] = templateMatchingIntegralImage(I,integralImages, row,...
    col,patchSize, searchWindowSize)

windowArea = searchWindowSize^2;

offsetsRows = zeros(windowArea,1);
offsetsCols = zeros(windowArea,1);
distances   = zeros(windowArea,1);

winWidth = floor(searchWindowSize/2);
winHeight = winWidth;

idx = 1;
for curRow = row - winWidth:row + winWidth
    for curCol = col - winHeight:col + winHeight
      
        idxX = curRow - row + winWidth + 1;
        idxY = curCol - col + winHeight + 1;

        integralImage = integralImages{idxX, idxY};   

        patchDistance = evaluateIntegralImage(integralImage, curRow, curCol, patchSize, searchWindowSize);

        distances(idx) = patchDistance;
        offsetsRows(idx) = curRow - row;
        offsetsCols(idx) = curCol - col;
        idx = idx + 1;
    end
end
end

function debugDistance(patchDistance)
handle = fopen('integral_distances.txt', 'a');
fprintf(handle, '%d \n', patchDistance);
fclose(handle);
end