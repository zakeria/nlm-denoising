function [ii] = computeIntegralImage(image, offsetX, offsetY, patchSize, winSize)

%REPLACE THIS
patchOffset = floor(patchSize/2);

ii = padarray(image, [winSize + patchOffset + 1, winSize + patchOffset + 1], 0, 'both');

differenceImage = imtranslate(ii, [offsetY, offsetX]);

% To compute the integral image we can simply compute the cumulative sum for each
% dimension of the image respectively.
% This works because e.g. if we compute the cumulative sum accross the
% rows, we now can perform a cumulative sum on the resulting image which is
% equivalent to the integral image for any pixel.
ii =  cumsum(cumsum((differenceImage - ii).^2, 2), 1);
end