% Perform bounds check.
% Get a pixel at a location - accounts for borders where I will be
% returning zero for them.
function pix = pixAt(I, i, j)
    [N,M] = size(I);
    if( i <= 0 || j <= 0 || i > N || j > M)
        pix = 0;
    else 
        pix = I(i,j);
    end
end